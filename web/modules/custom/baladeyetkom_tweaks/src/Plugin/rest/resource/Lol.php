<?php

namespace Drupal\baladeyetkom_tweaks\Plugin\rest\resource;

use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Drupal\poll\Entity\Poll;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Provides a Poll Resource
 *
 * @RestResource(
 *   id = "lol",
 *   label = @Translation("Lol"),
 *   uri_paths = {
 *     "canonical" = "/api/v1/api"
 *   }
 * )
 */
class Lol extends ResourceBase {

  /**
   * Responds to entity GET requests.
   * @return \Drupal\rest\ResourceResponse
   */
  public function get($pid) {
    return new JsonResponse(0);
  }

}
