<?php

namespace Drupal\baladeyetkom_tweaks\Plugin\rest\resource;

use Drupal\rest\Plugin\ResourceBase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\user\Entity\User;

/**
 * Provides custom user registration
 *
 * @RestResource(
 *   id = "custom_user_registration",
 *   label = @Translation("Custom user registration"),
 *   uri_paths = {
 *     "create" = "/api/v1/custom-user-registration"
 *   }
 * )
 */
class UserRegister extends ResourceBase {

  /**
   * Responds to entity Post requests.
   * @return \Drupal\rest\ResourceResponse
   */
  public function post($data) {

    $response = [];

    if (!isset($data['name']) && !isset($data['mail']) && !isset($data['pass'])) {
      $response = ['message' => t("Invalid user information"), 'status' => 0];
      return new JsonResponse($response);
    }

    if (isset($data['name']) && isset($data['mail']) && user_load_by_name($data['name']) && user_load_by_mail($data['mail'])) {
      $response = ['message' => t("The username and email are already in use"), 'status' => 0];
      return new JsonResponse($response);
    }

    if (isset($data['name']) && user_load_by_name($data['name'])) {
      $response = ['message' => t("The username is already in use"), 'status' => 0];
      return new JsonResponse($response);
    }

    if (isset($data['mail']) && user_load_by_mail($data['mail'])) {
      $response = ['message' => t("The email is already in use"), 'status' => 0];
      return new JsonResponse($response);
    }

    if (isset($data['field_phone'])) {
      $query = \Drupal::database()->select('user__field_phone', 'p');
      $query->addField('p', 'field_phone_value');
      $query->condition('p.field_phone_value', $data['field_phone']);
      $query->range(0, 1);

      if (!is_null($query->execute()->fetchObject()->field_phone_value)) {
        $response = ['message' => t("The phone is already in use"), 'status' => 0];
        return new JsonResponse($response);
      }
    }

    $user = User::create([
        'field_first_name' => $data['field_first_name'],
        'field_last_name' => $data['field_last_name'],
        'field_phone' => $data['field_phone'],
        'field_municipality' => $data['field_municipality'],
        'name' => $data['name'],
        'mail' => $data['mail'],
        'pass' => $data['pass'],
        'status' => 1
    ]);

    $user->save();
    $response = ['message' => t("User created successfully"), 'status' => 1];
    return new JsonResponse($response);
  }

}
