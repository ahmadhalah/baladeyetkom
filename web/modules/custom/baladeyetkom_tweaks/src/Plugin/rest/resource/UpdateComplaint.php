<?php

namespace Drupal\baladeyetkom_tweaks\Plugin\rest\resource;

use Drupal\rest\Plugin\ResourceBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\node\Entity\Node;
use Drupal\Core\Session\AccountProxyInterface;
use Psr\Log\LoggerInterface;

/**
 * Provides Update complaint
 *
 * @RestResource(
 *   id = "update_complaint",
 *   label = @Translation("Update Complaint"),
 *   uri_paths = {
 *     "create" = "/api/v1/update-complaint"
 *   }
 * )
 */
class UpdateComplaint extends ResourceBase {

  /**
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $account;

  /**
   * Constructs a Drupal\rest\Plugin\ResourceBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Session\AccountProxyInterface $account
   *   The current user.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, array $serializer_formats, LoggerInterface $logger, AccountProxyInterface $account) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);

    $this->account = $account;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration, $plugin_id, $plugin_definition, $container->getParameter('serializer.formats'), $container->get('logger.factory')->get('rest'), $container->get('current_user')
    );
  }

  /**
   * Responds to entity Post requests.
   * @return \Drupal\rest\ResourceResponse
   */
  public function post($data) {

    $response = [];
    $files = [];

    foreach ($data['files'] as $key => $value) {
      $file = file_save_data(base64_decode($value["image"]), 'public://' . date("Y-m-d") . '.' . $value['type']);
      $files[] = $file;
    }

    $node = Node::load($data['nid']);

    if (isset($data['body'])) {
      $node->set("body", $data['body']);
    }

    if (isset($data['title'])) {
      $node->set("title", $data['title']);
    }

    if (isset($data['detailed_location'])) {
      $node->set("field_plain_long", $data['detailed_location']);
    }

    if (isset($data['service_nid'])) {
      $node->set("field_category", $data['service_nid']);
    }

    if (isset($data['location_nid'])) {
      $node->set("field_node_location", $data['location_nid']);
    }

    if (isset($data['files'])) {
      $node->set("field_images", $files);
    }

    $node->set("field_status", 1); // Per review status

    $node->save();
    $response = ['message' => t("Complaint updated successfully"), 'status' => 1];
    return new JsonResponse($response);
  }

}
