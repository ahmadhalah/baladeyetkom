<?php

namespace Drupal\baladeyetkom_tweaks\Plugin\rest\resource;

use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Provides a Check Username and Email Resource
 *
 * @RestResource(
 *   id = "check_name_email",
 *   label = @Translation("Check Username and Email"),
 *   uri_paths = {
 *     "create" = "/api/v1/check-name-email"
 *   }
 * )
 */
class CheckNameEmailResource extends ResourceBase {

  /**
   * Responds to entity Post requests.
   * @return \Drupal\rest\ResourceResponse
   */
  public function post($data) {

    $response = [];
    if (isset($data['name']) && user_load_by_name($data['name'])) {
      $response[] = ['name' => '1'];
    }
    else {
      $response[] = ['name' => '0'];
    }

    if (isset($data['mail']) && user_load_by_mail($data['mail'])) {
      $response[] = ['mail' => '1'];
    }
    else {
      $response[] = ['mail' => '0'];
    }

    if (isset($data['phone'])) {
      $query = \Drupal::database()->select('user__field_phone', 'p');
      $query->addField('p', 'field_phone_value');
      $query->condition('p.field_phone_value', $data['phone']);
      $query->range(0, 1);

      if (!is_null($query->execute()->fetchObject()->field_phone_value)) {
        $response[] = ['phone' => '1'];
      }
      else {
        $response[] = ['phone' => '0'];
      }
    }
    else {
      $response[] = ['phone' => '0'];
    }

    return new JsonResponse($response);
  }

}
