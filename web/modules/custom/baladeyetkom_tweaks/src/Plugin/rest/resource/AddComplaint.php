<?php

namespace Drupal\baladeyetkom_tweaks\Plugin\rest\resource;

use Drupal\rest\Plugin\ResourceBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\node\Entity\Node;
use Drupal\Core\Session\AccountProxyInterface;
use Psr\Log\LoggerInterface;

/**
 * Provides custom Complaint add
 *
 * @RestResource(
 *   id = "add_complaint",
 *   label = @Translation("Add Complaint content"),
 *   uri_paths = {
 *     "create" = "/api/v1/add-complaint"
 *   }
 * )
 */
class AddComplaint extends ResourceBase {

    /**
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $account;

  /**
   * Constructs a Drupal\rest\Plugin\ResourceBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Session\AccountProxyInterface $account
   *   The current user.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, array $serializer_formats, LoggerInterface $logger, AccountProxyInterface $account) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);

    $this->account = $account;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('rest'),
      $container->get('current_user')
    );
  }

  /**
   * Responds to entity Post requests.
   * @return \Drupal\rest\ResourceResponse
   */
  public function post($data) {

    $response = [];
    $files = [];

    foreach ($data['files'] as $key => $value) {
      $file = file_save_data(base64_decode($value["image"]), 'public://' . date("Y-m-d") . '.' . $value['type']);
      $files[] = $file;
    }

    $node = Node::create([
      'title' => $data['title'],
      'body' => $data['body'],
      'uid' => $this->account->id(),
      'field_plain_long'=> $data['detailed_location'],
      'status' => 1,
      'field_status'=>1,                                     //default status {pending}
      'type' => 'complaint',
      'field_images' => $files,
      'field_node_location' => $data['location_nid'],
      'field_category' => $data['service_nid']
    ]);

    $node->save();
    $response = ['message' => t("Your Complaint has been created"),'status' =>1];
    return new JsonResponse($response);
  }

}
