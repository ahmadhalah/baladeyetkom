<?php

namespace Drupal\baladeyetkom_tweaks\Plugin\rest\resource;

use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\user\Entity\User;

/**
 * Provides a Forget Password Resource
 *
 * @RestResource(
 *   id = "forget_password",
 *   label = @Translation("Forget Password"),
 *   uri_paths = {
 *     "create" = "/api/v1/forget-password/{user}"
 *   }
 * )
 */
class ForgetPasswordResource extends ResourceBase {

  /**
   * Responds to entity GET requests.
   * @return \Drupal\rest\ResourceResponse
   */
  public function post($user) {
    $send_mail = FALSE;
    $new_pass = user_password();

    if ($account = user_load_by_name($user)) {
      if ($account) {
        $account->pass = $new_pass ;
        $account->save();
        $send_mail = TRUE;
      }
    }
    elseif ($account = user_load_by_mail($user)) {
      if ($account) {
        $account->pass = $new_pass;
        $account->save();
        $send_mail = TRUE;
      }
    }

    if ($send_mail) {
      $mailManager = \Drupal::service('plugin.manager.mail');
      $to = $account->getEmail();
      $params['message'] = t('Your new Password for Baladeyetkom App is @pass', ['@pass' => $new_pass]);
      $params['sender'] = 'Baladeyetkom App';
      $mailManager->mail('baladeyetkom_tweaks', 'bal_rest_password', $to, 'en', $params, NULL, TRUE);
    }
    $response = ['message' => t('A message with further instructions has been sent to your email address'),'status' =>1];

    return new JsonResponse($response);
  }

}
