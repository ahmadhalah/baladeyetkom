<?php

namespace Drupal\baladeyetkom_tweaks\Plugin\rest\resource;

use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Drupal\poll\Entity\Poll;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Provides a Poll Resource
 *
 * @RestResource(
 *   id = "poll_resource",
 *   label = @Translation("Poll Resource"),
 *   uri_paths = {
 *     "canonical" = "/api/v1/poll/{pid}"
 *   }
 * )
 */
class PollResource extends ResourceBase {

  /**
   * Responds to entity GET requests.
   * @return \Drupal\rest\ResourceResponse
   */
  public function get($pid) {
    $poll = Poll::load($pid);

    if ($poll->hasUserVoted()) {
      $response = [];
      $votes = $poll->getVotes();
      $total_votes = 0;

      foreach ($votes as $key => $value) {
        $total_votes += $value;
      }

      $response['has_user_voted'] = 1;
      $response['label'] = $poll->question->getValue()[0]['value'];
      $response['options'] = $poll->getOptions();
      $response['options_voting'] = $votes;
      $response['total_voting'] = $total_votes;
      return new JsonResponse($response);
    }
    else {
      $response = [];
      $response['has_user_voted'] = 0;
      $response['label'] = $poll->question->getValue()[0]['value'];
      $response['options'] = $poll->getOptions();
      return new JsonResponse($response);
    }
  }

}
