<?php

namespace Drupal\baladeyetkom_tweaks\Plugin\rest\resource;

use Drupal\rest\Plugin\ResourceBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\user\Entity\User;
use Drupal\Core\Session\AccountProxyInterface;
use Psr\Log\LoggerInterface;

/**
 * Provides custom user
 *
 * @RestResource(
 *   id = "update_user",
 *   label = @Translation("Update user "),
 *   uri_paths = {
 *     "create" = "/api/v1/update-user"
 *   }
 * )
 */
class UpdateUser extends ResourceBase {

  /**
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $account;

  /**
   * Constructs a Drupal\rest\Plugin\ResourceBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Session\AccountProxyInterface $account
   *   The current user.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, array $serializer_formats, LoggerInterface $logger, AccountProxyInterface $account) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);

    $this->account = $account;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration, $plugin_id, $plugin_definition, $container->getParameter('serializer.formats'), $container->get('logger.factory')->get('rest'), $container->get('current_user')
    );
  }

  /**
   * Responds to entity Post requests.
   * @return \Drupal\rest\ResourceResponse
   */
  public function post($data) {

    $user = User::load($data['user_id']);

    if (isset($data['name']) && $user->getUsername() != $data['name']) {
      if (user_load_by_name($data['name'])) {
        $response = ['message' => t("The username is already in use"), 'status' => 0];
        return new JsonResponse($response);
      }
    }

    if (isset($data['mail']) && $user->getEmail() != $data['mail']) {
      if (user_load_by_mail($data['mail'])) {
        $response = ['message' => t("The email is already in use"), 'status' => 0];
        return new JsonResponse($response);
      }
    }

    if (isset($data['field_phone']) && $data['field_phone'] != $user->get('field_phone')->getValue()[0]['value']) {
      $query = \Drupal::database()->select('user__field_phone', 'p');
      $query->addField('p', 'field_phone_value');
      $query->condition('p.field_phone_value', $data['field_phone']);
      $query->range(0, 1);

      if (!is_null($query->execute()->fetchObject()->field_phone_value)) {
        $response = ['message' => t("The phone is already in use"), 'status' => 0];
        return new JsonResponse($response);
      }
    }


    if (isset($data['field_firebase_token'])) {
      $user->set("field_firebase_token", $data['field_firebase_token']);
    }

    if (isset($data['field_first_name'])) {
      $user->set("field_first_name", $data['field_first_name']);
    }

    if (isset($data['field_last_name'])) {
      $user->set("field_last_name", $data['field_last_name']);
    }

    if (isset($data['field_phone'])) {
      $user->set("field_phone", $data['field_phone']);
    }

    if (isset($data['mail'])) {
      $user->setEmail($data['mail']);
    }

    if (isset($data['name'])) {
      $user->setUsername($data['name']);
    }

    if (isset($data['pass'])) {
      $user->setPassword($data['pass']);
    }

    $user->save();

    $response = ['message' => t("Information updated successfully"), 'status' => 1];
    return new JsonResponse($response);
  }

}
