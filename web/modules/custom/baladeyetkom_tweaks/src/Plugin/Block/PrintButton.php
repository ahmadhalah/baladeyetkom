<?php

namespace Drupal\baladeyetkom_tweaks\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'Print button' block.
 *
 * @Block(
 *   id = "print_button",
 *   admin_label = @Translation("Print Button"),
 *   category = @Translation("Bal")
 * )
 */
class PrintButton extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    if (\Drupal::routeMatch()->getRouteName() != 'entity.node.canonical') {
      return;
    }

    $node = \Drupal::routeMatch()->getParameter('node');
    if ($node instanceof \Drupal\node\NodeInterface) {
      // You can get nid and anything else you need from the node object.
      $nid = $node->id();
    }

    $html = [
      '#markup' => "<a target='_blank' href='/node/$nid/print'>" . t('Print Complaint') . "</a>",
    ];

    return [
      '#type' => 'markup',
      '#markup' => drupal_render($html),
      '#cache' => ['max-age' => 0],
    ];
  }

}
