<?php

namespace Drupal\baladeyetkom_tweaks\Controller;

use Drupal\Core\Controller\ControllerBase;
use TCPDF_FONTS;
use TCPDF;

/**
 * Controller class for Convert node to PDF.
 */
class PdfController extends ControllerBase {

  /**
   * Convert node PDF version.
   *
   * @param string $node
   *   The Node ID.
   */
  public function viewPrint($node) {
    $storage = \Drupal::entityTypeManager()->getStorage('node');
    $term_storage = \Drupal::entityTypeManager()->getStorage('taxonomy_term');
    $node = $storage->load($node);
    $user = $node->getOwner();
    $term_id = $user->field_municipality->getValue()[0]["target_id"];
    $term = $term_storage->load($term_id);
    $full_name = $user->field_first_name->getValue()[0]['value'] . '&nbsp;' . $user->field_last_name->getValue()[0]['value'];
    $phone = $user->field_phone->getValue()[0]['value'];
    $address = $node->field_plain_long->getValue()[0]['value'];
    $post_date = date('d-m-Y', $node->getCreatedTime());
    $id = $node->field_category->getValue()[0]['target_id'];
    $title = $term_storage->load($id)->getName();
    $id = $node->field_node_location->getValue()[0]['target_id'];
    $location = $storage->load($id)->getTitle();
    $details = '<table border="0" style="padding-top: 15px; padding-bottom: 15px;"><tr><td>' . $node->getTitle() . "<br>" . $node->body->getValue()['0']['value'] . '</td></tr></table>';
    $c_id = $node->field_id->getValue()['0']['value'];

    $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, 'A4', TRUE, 'UTF-8', FALSE);
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
    $pdf->setPrintHeader(FALSE);
    $pdf->setPrintFooter(FALSE);
    $pdf->SetMargins(18, 25, 18, TRUE);

    // set some language dependent data:
    $lg = Array();
    $lg['a_meta_charset'] = 'UTF-8';
    $lg['a_meta_dir'] = 'rtl';
    $lg['a_meta_language'] = 'fa';
    $lg['w_page'] = 'page';

    // set some language-dependent strings (optional)
    $pdf->setLanguageArray($lg);

    $pdf->SetFont('dejavusans', '', 12);


    $pdf->AddPage();
    $pdf->SetAutoPageBreak(TRUE, 0);

    $pdf->writeHTML('تسجيل الشكوى', TRUE, FALSE, TRUE, FALSE, 'C');
    $this->newLine($pdf);

    $pdf->writeHTML('بلدية ' . $term->getName(), TRUE, FALSE, TRUE, FALSE, 'C');
    $this->newLine($pdf);

    $pdf->writeHTML('التاريخ ' . date('d-m-Y'), TRUE, FALSE, TRUE, FALSE, 'R');
    $this->newLine($pdf);

    $pdf->writeHTML('الرقم التسلسلي للشكوى: ' . $c_id, TRUE, FALSE, TRUE, FALSE, 'R');
    $this->newLine($pdf);

    $pdf->writeHTML('الاسم الرباعي لمقدم الشكوى: ' . $full_name . '&nbsp;&nbsp;&nbsp;&nbsp;' . ' الرقم الوطني: ...................', TRUE, FALSE, TRUE, FALSE, 'R');
    $this->newLine($pdf);

    $pdf->writeHTML('العنوان: مكان الاقامة : ' . $term->getName() . '&nbsp;&nbsp;&nbsp;&nbsp;' . ' رقم الهاتف: ' . $phone, TRUE, FALSE, TRUE, FALSE, 'R');
    $this->newLine($pdf);

    $pdf->writeHTML('تاريخ تقديم الشكوى: ' . $post_date . '&nbsp;&nbsp;&nbsp;&nbsp;', TRUE, FALSE, TRUE, FALSE, 'R');
    $this->newLine($pdf);

    $pdf->writeHTML('موضوع الشكوى: ' . $title, TRUE, FALSE, TRUE, FALSE, 'R');
    $this->newLine($pdf);

    $pdf->writeHTML('مكان وقوع الشكوى: ' . $address . '&nbsp;&nbsp;&nbsp;&nbsp;' . ' الحي: ' . $location, TRUE, FALSE, TRUE, FALSE, 'R');
    $this->newLine($pdf);

    $pdf->writeHTML('الشارع: ' . $address . '&nbsp;&nbsp;&nbsp;&nbsp;', TRUE, FALSE, TRUE, FALSE, 'R');
    $this->newLine($pdf);

    $pdf->writeHTML('التفاصيل: ', TRUE, FALSE, TRUE, FALSE, 'R');
    $this->newLine($pdf);

    $pdf->writeHTMLCell($pdf->getPageWidth() - 36, '', '', '', $details, 1, 1, 0, TRUE, 'R', TRUE);
    $this->newLine($pdf);

    $pdf->writeHTML('اسم موظف الشكوى: ...................................................&nbsp;&nbsp;&nbsp;&nbsp; توقيعه: ....................', TRUE, FALSE, TRUE, FALSE, 'R');
    $this->newLine($pdf);

    $pdf->writeHTML('مشروحات رئيس قسم العلاقات العامة / الشكاوى: ', TRUE, FALSE, TRUE, FALSE, 'R');
    $this->newLine($pdf);

    $pdf->writeHTMLCell($pdf->getPageWidth() - 36, '', '', '', '<table border="0" style="padding-top: 80px; padding-bottom: 80px;"><tr><td></td></tr></table>', 1, 1, 0, TRUE, 'R', TRUE);
    $this->newLine($pdf);

    $pdf->writeHTML('الاسم: ................................... التاريخ: ........................ التوقيع: ............................', TRUE, FALSE, TRUE, FALSE, 'R');

    $pdf->lastPage();
    $pdf->Output("test.pdf", 'I');

  }

  /**
   * Get no results behavior.
   */
  public function newLine($pdf) {
    $pdf->SetY($pdf->GetY() + 6);
  }

}