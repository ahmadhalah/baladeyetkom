<?php

namespace Drupal\baladeyetkom_tweaks\Controller;

use Drupal\Core\Controller\ControllerBase;

class UserReports extends ControllerBase {

  public function content() {
    $terms = ['Maan' => 7, 'Ramtha' => 15, 'Taypeh' => 14, 'Mafraq' => 13, 'Alwasateyeh' => 16];
    $rows = [];
    $rowsDate = [];
    $total = 0;
    $totalDate = 0;
    $date = 1526860800;
    foreach ($terms as $key => $value) {
      $count = $this->getCount($key, $value);
      $countDate = $this->getCountDate($date, $value);

      if ($count) {
        $total += $count->count;
        $row = [
          'muncipility' => $key,
          'count' => $count->count
        ];
        $rows[] = $row;
      }

      if ($countDate) {
        $totalDate += $countDate->count;
        $rowDate = [
          'muncipility' => $key,
          'count' => $countDate->count
        ];
        $rowsDate[] = $rowDate;
      }
    }

    $row = [
      'muncipility' => 'Total',
      'total' => $total
    ];

    $rows[] = $row;

    $rowDate = [
      'muncipility' => 'Total',
      'total' => $totalDate
    ];

    $rowsDate[] = $rowDate;

    return [
      'reports_table' => [
        '#caption' => 'From start to now',
        '#theme' => 'table',
        '#header' => $this->getTableHeader(),
        '#rows' => $rows
      ],
      'date_table' => [
        '#caption' => 'From 21 May, 2018',
        '#theme' => 'table',
        '#header' => $this->getTableHeader(),
        '#rows' => $rowsDate
      ]
    ];
  }

  protected function getCount($key, $value) {
    $connection = \Drupal::database();
    $query = $connection->query("SELECT count(users_field_data.uid) AS count FROM users_field_data LEFT JOIN user__roles"
            . " ON users_field_data.uid = user__roles.entity_id INNER JOIN user__field_municipality ON users_field_data.uid ="
            . " user__field_municipality.entity_id WHERE (user__roles.roles_target_id IS NULL) AND "
            . "(user__field_municipality.field_municipality_target_id = '$value')");

    return $query->fetch();
  }

  protected function getCountDate($date, $value) {
    $connection = \Drupal::database();
    $query = $connection->query("SELECT count(users_field_data.uid) "
            . "AS count FROM users_field_data LEFT JOIN user__roles ON "
            . "users_field_data.uid = user__roles.entity_id INNER JOIN "
            . "user__field_municipality ON users_field_data.uid = "
            . "user__field_municipality.entity_id WHERE "
            . "(user__roles.roles_target_id IS NULL) AND (users_field_data.created > $date) "
            . "AND (user__field_municipality.field_municipality_target_id = '$value')");

    return $query->fetch();
  }

  protected function getTableHeader() {
    return [
      'muncipility' => [
        'data' => $this->t('Muncipility'),
      ],
      'last' => [
        'data' => $this->t('Count'),
      ],
    ];
  }

}
