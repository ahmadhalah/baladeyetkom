/**
 * @file
 * Behaviors for the baladeyetkom admin theme.
 */

(function ($, _, Drupal, drupalSettings) {
  'use strict';

  Drupal.behaviors.baladeyetkomAdmin = {
    attach: function (context) {
      // Baladeyetkom admin JavaScript behaviors goes here.
    }
  };

})(window.jQuery, window._, window.Drupal, window.drupalSettings);
